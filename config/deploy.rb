# lock to current capistrano version
lock "3.8.1"

set :application, "crossover_event_site"
set :repo_url, "erik_poehler@bitbucket.org:erik_poehler/crossover_event_site.git"
ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

set :user, "deploy"
set :use_sudo, true
set :deploy_to, '/var/www/mx.teuton.crossover-eventsite'
set :keep_releases, 10

# run composer update
namespace :composer do
    before 'install', 'change_dir'

    desc 'Composer update'
    task :change_dir do
        on roles(:app) do
            execute "cd #{release_path}/ && composer update"
        end
    end
end