<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="xsl xsi html xsd" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*" />
    <xsl:param name="fullname" />
    <xsl:param name="loggedin" />
    <xsl:param name="message" />
    
    <xsl:template match="/">
        <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Index</title>
            <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no" />
            <link rel="stylesheet" type="text/css" href="/assets/css/main.min.css" />
        </head>
        <body>
            <div class="container">
                <div class="row">
                    <div class="col col-sm-12" style="margin-bottom:40px;">
                        <h1 class="text-center">My Amazing Event Site</h1>
                        <div id="map" style="width: 100%; height: 400px;"></div>
                    </div>
                </div>
            </div>
            <xsl:for-each select="/root/Event">
                <div class="col-sm-12 event" id="event-{@id}" data-latitude="{Venue/locality_latitude}" data-longitude="{Venue/locality_longitude}" data-venue-id="{Venue/@id}">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3">
                                    <img class="rounded img-thumbnail" src="{logo}" alt="Card image cap" title="" />
                                </div>
                                <div class="col-9">
                                    <h4 class="card-title"><xsl:value-of select="title" /></h4>
                                    <h6 class="card-subtitle mb-2 text-muted"><xsl:value-of select="subtitle" /></h6>
                                    <p class="card-text"><xsl:value-of select="description" /></p>
                                    <address class="vcard" itemprop="address" itemscope="itemscope" itemtype="http://schema.org/PostalAddress"> 
                                       <strong>Venue:</strong><xsl:text> </xsl:text><span class="fn title"><xsl:value-of select="Venue/name" /></span>,<xsl:text> </xsl:text>
                                       <span class="adr"> 
                                          <span class="street-address" itemprop="streetAddress">9th Street, 9</span><xsl:text> </xsl:text>
                                          <span class="locality" itemprop="addressLocality" ><xsl:value-of select="Venue/locality_name" /></span>,<xsl:text> </xsl:text>
                                          <span class="region" itemprop="addressRegion"><abbr title="California">CA</abbr></span><xsl:text> </xsl:text>
                                          <span class="postal-code" itemprop="postalCode">95113</span><xsl:text> </xsl:text>
                                          <span class="country">USA</span>
                                       </span>
                                    </address>
                                    <a href="/book/{@id}" class="btn btn-primary">Book your place</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </xsl:for-each>
            <script type="text/javascript" src="/assets/js/mapbox-gl.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript" src="/assets/js/jquery-3.2.1.min.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript" src="/assets/js/behavior.min.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript">
var geojson = {
  type: 'FeatureCollection',
  features: [<xsl:for-each select="/root/Event/Venue">
  {
    type: 'Feature',
    geometry: {
      type: 'Point',
      coordinates: [<xsl:value-of select="locality_longitude" />, <xsl:value-of select="locality_latitude" />]  
    },
    properties: {
      title: '<xsl:value-of select="name" />',
      id: '<xsl:value-of select="../@id" />'
    }
  }<xsl:if test="position() != last()">,</xsl:if>
  </xsl:for-each>
]};
/** // add markers to map
geojson.features.forEach(function(marker) {

  // create a HTML element for each feature
  var el = document.createElement('div');
  el.className = 'marker';
  el.setAttribute("data-event-id", marker.properties.id); 

  // make a marker for each feature and add to the map
  new mapboxgl.Marker(el)
  .setLngLat(marker.geometry.coordinates)
  .addTo(map);
}); **/
map.flyTo({center: e.features[0].geometry.coordinates});
map.addLayer({
        "id": "symbols",
        "type": "symbol",
        "source": {
            "type": "geojson",
            "data": {
                "type": "FeatureCollection",
                "features": geojson
            }
        }
    });
                        </script>
        </body>
        </html>
    </xsl:template>
</xsl:stylesheet>