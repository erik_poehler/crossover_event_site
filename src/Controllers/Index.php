<?php
/**
 * Index Controller
 *
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 */
declare(strict_types = 1);

namespace EventSite\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use EventSite\Template\XsltRenderer;

class Index extends Controller
{
    /**
     *
     * @var Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     *
     * @var Symfony\Component\HttpFoundation\Response
     */
    protected $response;

    /**
     *
     * @var EventSite\Template\Renderer
     */
    protected $renderer;

    /**
     *
     * @var Symfony\Component\HttpFoundation\Session\Session
     */
    protected $session;

    /**
     *
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param Symfony\Component\HttpFoundation\Response $response
     * @param Symfony\Component\HttpFoundation\Session $session
     * @param EventSite\Template\XsltRenderer $renderer
     */
    public function __construct(
        Request $request,
        Response $response,
        Session $session,
        XsltRenderer $renderer
    ) {
            $this->request = $request;
            $this->response = $response;
            $this->session = $session;
            $this->renderer = $renderer;
    }

    /**
     * Shows the Index Page
     * @param array $vars
     * @return Response
     */
    public function index() : Response
    {
        $this->session->set('user/fullname', $this->getUserFullname('Guest'));

        $this->session->getFlashBag()->add('loggedin', '0');
        $this->session->getFlashBag()->add('fullname', 'Guest');

        $params = $this->getParameters();

        $html = $this->renderer->render('eventsite::index', 'default', [], false);
        $this->response->setContent($html);
        return $this->response;
    }

    /**
     * Logs out the user and redirects him back to the index() page
     * @param unknown $vars
     * @return RedirectResponse
     */
    public function logout() : RedirectResponse
    {
        $this->session->clear();
        return new RedirectResponse('/');
    }

    /**
     *
     * @return Response
     */
    public function login() : Response
    {
        $this->session->clear();
        return new RedirectResponse('/');
    }
}
