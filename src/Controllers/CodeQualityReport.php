<?php

namespace EventSite\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EventSite\Template\XsltRenderer;

class CodeQualityReport extends Controller
{

    protected $request;
    protected $response;
    protected $renderer;

    public function __construct(Request $request, Response $response, XsltRenderer $renderer)
    {
        $this->request = $request;
        $this->response = $response;
        $this->renderer = $renderer;
    }

    /**
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        /**
         * @todo only show this to the admin
         * @var array $reports
         */
        $reports = glob(__DIR__ . '/../../cache/php*.xml');
        $realpath_base = realpath(__DIR__ . '/../../');
        $html = 'No reports found';
        if ($reports) {
            $dom = new \DOMDocument();
            $dom->loadXML('<root />');
            foreach ($reports as $report) {
                $reportDOM = new \DOMDocument();
                $reportDOM->load($report);
                $node = $reportDOM->documentElement;
                $node->setAttribute('report', ucwords(str_replace(['phpmd.','.xml'], '', basename($report))));
                $files = $reportDOM->getElementsByTagName('file');
                if ($files->length) {
                    foreach ($files as $file) {
                        $name = $file->getAttribute('name');
                        $file->setAttribute('name', str_replace($realpath_base, '', $name));
                    }
                }
                $reportNode = $dom->importNode($node, true);
                $dom->documentElement->appendChild($reportNode);
            }
            file_put_contents(__DIR__ . '/../../cache/report.xml', $dom->saveXML());
            $html = $this->renderer->render('eventsite::phpmd', 'default', [], false, $dom);
        }
        $this->response->setContent($html);
        return $this->response;
    }
}
