<?php
/**
 * Dependency Management
 *
 * @desc     Defines dependencies for various required classes
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 */
declare(strict_types = 1);

namespace EventSite;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Tools\Setup;
use Doctrine\DBAL\Connection;

$injector = new \Auryn\Injector();

$injector->alias('Symfony\Component\HttpFoundation\Request', 'Symfony\Component\HttpFoundation\Request');
$injector->share('Symfony\Component\HttpFoundation\Request');
$injector->define(
    'Symfony\Component\HttpFoundation\Request',
    [
        ':get' => $_GET,
        ':post' => $_POST,
        ':request' => [],
        ':cookies' => $_COOKIE,
        ':files' => $_FILES,
        ':server' => $_SERVER,
    ]
);

$complexClassFactory = function () {
    $config = include __DIR__ . '/../config/config.php';
    $connection = [
        'dbname' => $config['db']['database'],
        'user' => $config['db']['user'],
        'password' => $config['db']['pass'],
        'host' => $config['db']['host'],
        'driver' => $config['db']['driver'],
        'charset' => 'utf8mb4',
        'default_table_options' => [
            'charset' => 'utf8mb4',
            'collate' => 'utf8mb4_unicode_ci',
        ],
    ];
    \Doctrine\DBAL\Types\Type::addType('uuid', '\Ramsey\Uuid\Doctrine\UuidType');
    \Doctrine\DBAL\Types\Type::addType('point', '\CrEOF\Spatial\DBAL\Types\Geometry\PointType');
    $configuration = Setup::createAnnotationMetadataConfiguration([__DIR__."/src"], true);
    $entityManager = EntityManager::create($connection, $configuration);
    return $entityManager;
};

$injector->alias('EntityManager', 'Doctrine\ORM\EntityManager');
$injector->delegate('Doctrine\ORM\EntityManager', $complexClassFactory);
$injector->share('EntityManager');

$injector->alias('Symfony\Component\HttpFoundation\Response', 'Symfony\Component\HttpFoundation\Response');
$injector->share('Symfony\Component\HttpFoundation\Response');
$injector->alias('Symfony\Component\HttpFoundation\Session\Session', 'Symfony\Component\HttpFoundation\Session\Session');
$injector->share('Symfony\Component\HttpFoundation\Session\Session');
$injector->alias('EventSite\Template\RendererInterface', 'EventSite\Template\XsltRendererInterface');
$injector->share('EventSite\Template\XsltRenderer');
//

return $injector;
