<?php
/**
 * Event Entity
 *
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 *
 */
declare(strict_types = 1);

namespace EventSite\Entity;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use EventSite\Entity\EventBooking;
use EventSite\Entity\Venue;
use EventSite\Repository\EventRepository;

/**
 * @Entity
 * @Table(name="events")
 */
class Event
{
    const NODE_NAME = 'event';

    /**
     * @var \Ramsey\Uuid\Uuid
     *
     * @Id
     * @Column(type="uuid", unique=true, options={"comment":"Event ID"}, nullable=false)
     * @GeneratedValue(strategy="CUSTOM")
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $event_id;

    /**
     * @var string
     *
     * @Column(name="title", type="string", length=255, nullable=false, options={"comment":"Event title"})
     */
    private $title;

    /**
     * @var string
     *
     * @Column(name="subtitle", type="string", length=255, nullable=true, options={"comment":"Event Subtitle"})
     */
    private $subtitle;

    /**
     * @var string
     *
     * @Column(name="description", type="text", length=65535, nullable=true, options={"comment":"Event Description"})
     */
    private $description;

    /**
     * @var string
     *
     * @Column(name="logo", type="string", length=255, nullable=true, options={"comment":"Event Logo"})
     */
    private $logo;

    /**
     * @var EventSite\Entity\Venue
     *
     * @ManyToOne(targetEntity="Venue")
     * @JoinColumn(name="fk_venue", referencedColumnName="venue_id", nullable=false)
     */
    private $fk_venue;

    /**
     * @Column(type="datetime", nullable=true, options={"comment":"Date created"})
     * @var
     */
    private $created_at;

    /**
     *
     * @var array
     */
    private $bookings = [];

    /**
     *
     * @return EventSite\Entity\Venue
     */
    public function getVenue() : Venue
    {
        return $this->fk_venue;
    }

    /**
     *
     * @param string $title
     * @param EventSite\Entity\Venue $venue
     * @param string $subtitle
     * @param string $description
     * @param string $logo
     */
    public function __construct($title, Venue $venue, $subtitle = null, $description = null, $logo = null)
    {
        $this->event_id = Uuid::uuid4();
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->description = $description;
        $this->logo = $logo;
        $this->fk_venue = $venue;
        $this->created_at = new \DateTime("now");
    }

    /**
     *
     * @param string $title
     * @param EventSite\Entity\Venue $venue
     * @param string $subtitle
     * @param string $description
     * @param string $logo
     */
    public static function create($title, Venue $venue, $subtitle = null, $description = null, $logo = null)
    {
        $this->event_id = Uuid::uuid4();
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->description = $description;
        $this->logo = $logo;
        $this->fk_venue = $venue;
        $this->created_at = new \DateTime("now");
    }

    /**
     *
     * @return \Ramsey\Uuid\Uuid
     */
    public function getId()
    {
        return $this->event_id->__toString();
    }

    /**
     *
     * @param $user EventSite\Entity\User the user entity that books the event
     */
    public function book(User $user) : EventBooking
    {
        $booking = new EventBooking($user, $this);
        $this->bookings[] = $booking;
        return $booking;
    }

    /**
     * transform this event into an array
     * @return array
     */
    public function __toArray() : array
    {
        return [
            'event_id' => $this->event_id->__toString(),
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'description' => $this->description,
            'logo' => $this->logo,
            'venue' => $this->fk_venue->__toArray(),
            'bookings' => $this->bookings,
            'created_at' => $this->created_at->format(\DateTime::ISO8601),
        ];
    }

    /**
     * transforms this event into a domelement
     * @return \DOMElement
     */
    public function __toXML() : \DOMElement
    {
        $element = new \DOMElement(self::NODE_NAME);
        foreach ($this->toArray as $key => $value) {
            $child = new \DOMElement($key, $value);
            $element->appendChild($child);
        }
        return $element;
    }
}
