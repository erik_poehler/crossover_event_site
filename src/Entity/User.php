<?php
/**
 * User Entity
 *
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 *
 */
declare(strict_types = 1);

namespace EventSite\Entity;

use Ramsey\Uuid\Uuid;

/**
 * @Entity
 * @Table(name="users")
 */
class User
{
    /**
     * @var \Ramsey\Uuid\Uuid
     *
     * @Id
     * @Column(type="uuid", unique=true, nullable=false, options={"comment":"User ID"})
     * @GeneratedValue(strategy="CUSTOM")
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $user_id;

    /**
     * @Column(type="string", unique=true, nullable=false, options={"comment":"Username"})
     * @var string
     */
    private $username;

    /**
     * @Column(type="string", nullable=false, options={"comment":"Password"})
     * @var string
     */
    private $password;

    /**
     * @Column(type="string", nullable=false, options={"comment":"Email"})
     * @var string
     */
    private $email;

    /**
     * @Column(type="datetime", nullable=true, options={"comment":"Date created"})
     * @var
     */
    private $created_at;

    /**
     *
     * @param string $username
     * @param string $password
     * @param string $email
     * @return EventSite\Entity\User
     */
    public function __construct($username, $password, $email)
    {
        $this->user_id = Uuid::uuid4();
        $this->username = $username;
        $this->password = sha1($password);
        $this->email = $email;
        $this->created_at = new \DateTime("now");
    }

    /**
     *
     * @return string
     */
    public function getId()
    {
        return $this->user_id->__toString();
    }

    /**
     *
     * @return array
     */
    public function __toArray() : array
    {
        return [
            'user_id' => $this->user_id->__toString(),
            'username' => $this->username,
            'password' => $this->password,
            'email' => $this->email,
            'created_at' => $this->created_at->format(\DateTime::ISO8601),
        ];
    }
}
