<?php
/**
 * Venue Entity
 *
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 *
 */
declare(strict_types = 1);

namespace EventSite\Entity;

use Ramsey\Uuid\Uuid;
use EventSite\Entity\Address;
use CrEOF\Spatial\PHP\Types\Geography\Point;

/**
 * @Entity
 * @Table(name="venues")
 */
class Venue
{
    /**
     * @var \Ramsey\Uuid\Uuid
     *
     * @Id
     * @Column(type="uuid", unique=true, nullable=false, options={"comment":"Venue ID"})
     * @GeneratedValue(strategy="CUSTOM")
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $venue_id;

    /**
     * @Column(type="string", nullable=false, length=255, options={"comment":"Name of the venue"})
     * @var string
     */
    private $name;

    /**
     * @var EventSite\Entity\Address
     *
     * @ManyToOne(targetEntity="Address")
     * @JoinColumn(name="fk_address", referencedColumnName="address_id", nullable=false)
     */
    private $fk_address;

    /**
     * @Column(type="point", nullable=false, options={"comment":"Venue location as Geo Point"})
     * @var CrEOF\Spatial\PHP\Types\Geography\Point
     */
    private $locality_geo;

    /**
     * @Column(type="datetime", nullable=true, options={"comment":"Date created"})
     * @var
     */
    private $created_at;

    /**
     *
     * @param string $name
     * @param float $lat
     * @param float $long
     * @param \EventSite\Entity\Address $address
     * @return void
     */
    public function __construct($name, $lat, $long, Address $address)
    {
        $this->venue_id = Uuid::uuid4();
        $this->name = $name;
        $this->locality_geo = new Point($lat, $long);
        $this->fk_address = $address;
        $this->created_at = new \DateTime("now");
    }

    /**
     *
     * @return string
     */
    public function getId()
    {
        return $this->venue_id->__toString();
    }

    /**
     *
     * @return array
     */
    public function __toArray() : array
    {
        return [
            'venue_id' => $this->venue_id->__toString(),
            'name' => $this->name,
            'address' => $this->fk_address->__toArray(),
            'locality_latitude' => $this->locality_geo->getLatitude(),
            'locality_longitude' => $this->locality_geo->getLongitude(),
            'created_at' => $this->created_at->format(\DateTime::ISO8601),
        ];
    }
}
