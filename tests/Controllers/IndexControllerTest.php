<?php
/**
 * Controller Test
 *
 * @category Class
 * @package  Exam
 * @license  http://creativecommons.org/licenses/by-nc-nd/4.0/ Attribution-NonCommercial-NoDerivatives 4.0 International
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     http://www.erikpoehler.com/
 *
 */
declare(strict_types = 1);

namespace EventSiteTest\Controllers;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EventSite\Controllers\IndexController;
use EventSite\Template\XsltRenderer;
use Symfony\Component\HttpFoundation\Session\Session;
use PDO;
use EventSite\Models\Database;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

class IndexControllerTest extends TestCase
{
    public function testIndexResponse()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $indexController = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $this->assertTrue($indexController->show([]) instanceof Response);
    }
    public function testLogoutResponse()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $indexController = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $this->assertTrue($indexController->logout([]) instanceof RedirectResponse);
    }
}
