<?php

namespace EventSite\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EventSite\Entity\Event;
use EventSite\Entity\EventDate;
use EventSite\Entity\User;
use EventSite\Entity\Venue;
use EventSite\Entity\Address;
use Doctrine\ORM\EntityManager;

class Database extends Controller
{

    protected $request;
    protected $response;
    protected $entityManager;

    public function __construct(Request $request, Response $response, EntityManager $entityManager)
    {
        $this->request = $request;
        $this->response = $response;
        $this->entityManager = $entityManager;
    }

    public function index()
    {
        if ($this->entityManager->getRepository('EventSite\Entity\Event')->count([])) {
            $this->response->setStatusCode(Response::HTTP_NOT_ACCEPTABLE, 'Database already setup.');
            $this->response->setContent('Database already has data. <a href="/">Go to the homepage</a>');
        } else {
            /**
             *
             * Create Sample Data: Addresses
             */
            $address = [];
            $address[] = new Address('USA', null, 'San José', 'California', 'CA', 'San Carlos St', '150 W', '95113');
            $address[] = new Address('USA', null, 'San José', 'California', 'CA', 'Park Ave', '345', '95110');
            $address[] = new Address('USA', null, 'San José', 'California', 'CA', 'Santa Clara St', '525 W', '95113');
            foreach ($address as $addr) {
                $this->entityManager->persist($addr);
            }

            /**
             *
             * Create Sample Data: Venues
             */
            $venues = [];
            $venues[] = new Venue('McEnery Convention Center', -121.8887936, 37.329008, $address[0]);
            $venues[] = new Venue('Adobe', -121.8939647, 37.3306844, $address[1]);
            $venues[] = new Venue('SAP Center', -121.9012363, 37.3327303, $address[2]);
            foreach ($venues as $venue) {
                $this->entityManager->persist($venue);
            }

            /**
             *
             * Create Sample Data: Events
             */
            $events = [];
            $events[] = new Event('PHP ZendCon', $venues[0], 'Accelerating PHP & Open Source', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '/assets/images/zendcon-logo.png');
            $events[] = new Event('Adobe MAX', $venues[1], 'The Creativity Conference', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '/assets/images/maxlogo.png');
            $events[] = new Event('Netsuite One World', $venues[2], 'The Netsuite Conference', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '/assets/images/sap-logo.png');
            foreach ($events as $event) {
                $this->entityManager->persist($event);
            }

            /**
             *
             * Create Sample Data: User
             */
            $user = new \EventSite\Entity\User('user', 'user', 'admin@teuton.mx');
            $this->entityManager->persist($user);

            /**
             *
             * Create Sample Data: Event Booking
             */
            $booking = $events[0]->book($user);
            $this->entityManager->persist($booking);

            /**
             *
             * Create Sample Data: Event Date
             */
            $event_dates = [];
            $event_dates[] = new EventDate(new \DateTime('now'), EventDate::TYPE_START, $events[0]);
            $event_dates[] = new EventDate(new \DateTime('now + 1day'), EventDate::TYPE_END, $events[0]);
            foreach ($event_dates as $event_date) {
                $this->entityManager->persist($event_date);
            }

            $this->entityManager->flush(); // send everything to the DB

            $this->response->setContent('Data added to the DB successfully.');
        }
        return $this->response;
    }
}
