<?php
/**
* Application Bootstrap File
*
* @desc     autoloading, error handling, dependency injection, route dispatching
* @category Class
* @package  EventSite
* @license  CC-BY-NC-ND-4.0
* @author   Erik Pöhler <info@teuton.mx>
* @link     https://www.erikpoehler.com/
*/
declare(strict_types = 1);

namespace EventSite;

require __DIR__ . '/../vendor/autoload.php';

$du = new \EventSite\Helpers\DebugUtilities();
$du->startDuration();

error_reporting(E_ALL ^ E_CORE_WARNING);

$config = include __DIR__ . '/../config/config.php';
$environment = $config['environment'];

$whoops = new \Whoops\Run;
if ($environment !== 'production') {
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
} else {
    $whoops->pushHandler(
        function ($e) {
            // echo 'Todo: Friendly error page and send an email to the developer';
            // mail($config['emails']['admin_phpmail'], 'An Exception occurred in Production Environment', $e->__toString());
        }
    );
}
$whoops->register();

$injector = include 'Dependencies.php';
$request = $injector->make('Symfony\Component\HttpFoundation\Request');
$response = $injector->make('Symfony\Component\HttpFoundation\Response');
$session = $injector->make('Symfony\Component\HttpFoundation\Session\Session');

foreach ($response->headers->all() as $key => $header) {
    header("$key: $header[0]", false);
}

$routeDefinitionCallback = function (\FastRoute\RouteCollector $r) {
    $routes = include 'Routes.php';
    foreach ($routes as $route) {
        $r->addRoute($route[0], $route[1], $route[2]);
    }
};
$dispatcher = \FastRoute\simpleDispatcher($routeDefinitionCallback);

$routeInfo = $dispatcher->dispatch($request->getMethod(), strtok($request->getRequestUri(), '?'));

switch ($routeInfo[0]) {
    case \FastRoute\Dispatcher::NOT_FOUND:
        $response->setContent(file_get_contents(__DIR__ . '/../templates/error/404.html'));
        $response->setStatusCode(404);
        break;
    case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $response->setContent(file_get_contents(__DIR__ . '/../templates/error/405.html'));
        $response->setStatusCode(405);
        break;
    case \FastRoute\Dispatcher::FOUND:
        $className = $routeInfo[1][0];
        $method = $routeInfo[1][1];
        $class = $injector->make($className);
        $response = $class->$method();
        break;
}
$response->prepare($request);
$response->send();
?>

<!-- 
Profiling:
Memory Usage: <?php echo $du->bytesToSize(memory_get_usage(), 2)."\n"; ?>
Memory Peak Usage: <?php echo $du->bytesToSize(memory_get_peak_usage(), 2)."\n"; ?>
Included Files: <?php echo count(get_included_files())."\n"; ?>
<?php
$du->endDuration();
echo $du->showDurationStatistics(); ?>
-->