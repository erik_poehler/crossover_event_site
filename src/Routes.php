<?php
/**
 * Dependency Management
 *
 * @desc     Defines routes, http methods and controller classes (and their action methods)
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 */
declare(strict_types = 1);

namespace EventSite;

return [
    ['GET', '/', ['EventSite\Controllers\Index', 'index']],
    [['GET','POST'], '/login', ['EventSite\Controllers\Index', 'login']],
    ['GET', '/logout', ['EventSite\Controllers\Index', 'logout']],
    ['GET', '/database', ['EventSite\Controllers\Database', 'index']], // setup sample data
    ['GET', '/code-quality', ['EventSite\Controllers\CodeQualityReport', 'index']], // display phpmd results
];
