<?php
/**
 * Controller Test
 *
 * @category Class
 * @package  Exam
 * @license  http://creativecommons.org/licenses/by-nc-nd/4.0/ Attribution-NonCommercial-NoDerivatives 4.0 International
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     http://www.erikpoehler.com/
 *
 */
declare(strict_types = 1);

namespace EventSiteTest\Controllers;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EventSite\Controllers\IndexController;
use EventSite\Template\XsltRenderer;
use Symfony\Component\HttpFoundation\Session\Session;
use PDO;
use EventSite\Models\Database;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use InvalidArgumentException;
use TypeError;

class ControllerTest extends TestCase
{

    public function testIsLoggedIn()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $controller = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $this->assertFalse($controller->isLoggedIn());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testDoLoginEmptyArrayException()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $controller = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $this->expectException(InvalidArgumentException::class);

        $controller->doLogin([]);
    }

    /**
     * @expectedException TypeError
     */
    public function testDoLoginWrongTypeException()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $controller = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $this->expectException(TypeError::class);

        $controller->doLogin(true);
    }

    public function testDoLoginUserEquals()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $controller = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $user = ['user_id' => '1', 'username' => 'Dummy', 'fullname' => 'Dummy User'];
        $controller->doLogin($user);
        $this->assertEquals($user, $controller->getSession()->get('user'));
        $this->assertEquals('Dummy', $controller->getSession()->get('user')['username']);
    }

    public function testDoLogoutIsNull()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $controller = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $controller->doLogout();
        $this->assertNull($controller->getSession());
        $this->expectException('Error');
        $controller->getSession()->get('user'); // getSession() returns null
    }

    public function testGetUsernameMissingDefault()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $controller = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $controller->getUsername();
        $this->assertEquals('guest', $controller->getUsername());
    }

    public function testGetUsernameDefault()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $controller = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $controller->getUsername();
        $this->assertEquals('Dummy', $controller->getUsername('Dummy'));
    }

    public function testGetUsernameFromSession()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $controller = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $user = ['user_id' => '1', 'username' => 'erik', 'fullname' => 'Erik Pöhler'];
        $controller->doLogin($user);

        $controller->getUsername();
        $this->assertEquals('erik', $controller->getUsername('Dummy'));
    }

    public function testGetUserFullname()
    {
        $config = include __DIR__ . '/../../config/config.local.php';
        $session = new Session(new MockArraySessionStorage());
        $pdo = new PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass'], [$config['db']['options']]);
        $controller = new IndexController(new Request(['/']), new Response(), new XSLTRenderer(new \XSLTProcessor()), new Database($pdo), $session);

        $user = ['user_id' => '1', 'username' => 'erik', 'fullname' => 'Erik Pöhler'];
        $controller->doLogin($user);
        $this->assertEquals('Erik Pöhler', $controller->getUserFullname('Dummy'));
    }

    /**

    public function getUserFullname($default = 'Guest') : string
    {
        return (string) $this->session->get('user/fullname', $default);
    }**/
}
