<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="xsl xsi html xsd" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" indent="no" encoding="UTF-8" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*" />
    <xsl:param name="fullname" />
    <xsl:param name="loggedin" />
    <xsl:param name="message" />
    
    <xsl:template match="/">
        <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Index</title>
            <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no" />
            <link rel="stylesheet" type="text/css" href="/assets/css/main.min.css" />
            <style>
                .card-header {color:#FFF;}
            </style>
        </head>
        <body>
        
        <!-- <textarea style="width:100%;height:200px;" >
            <xsl:copy-of select="." />
        </textarea> -->
        
            <div class="container">
                <div class="row">
                    <div class="col col-sm-12" style="margin-bottom:40px;">
                        <h1 class="text-center">PHPMD Report</h1>
                        <xsl:for-each select="/root/pmd">
                            <div class="row">
                                <div class="col col-sm-12" style="margin-bottom:40px;">
                                    <div class="card">
                                        <div>
                                            <xsl:attribute name="class">
                                                card-header <xsl:if test="count(file) = '0'">bg-success</xsl:if>
                                                <xsl:if test="count(file) > '0'">bg-danger</xsl:if>
                                            </xsl:attribute>
                                            <strong><xsl:value-of select="file[1]/violation/@ruleset" /> <xsl:if test="count(file) = '0'"><xsl:value-of select="@report" /> Rules</xsl:if></strong>
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <xsl:if test="count(file) = '0'">
                                                <li class="list-group-item">No errors found.</li>
                                            </xsl:if>
                                            <xsl:apply-templates select="file" mode="phpmd"/>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </xsl:for-each>
                        <h1 class="text-center">PHPCS Report</h1>
                        <xsl:for-each select="/root/phpcs">
                            <div class="row">
                                <div class="col col-sm-12" style="margin-bottom:40px;">
                                    <div class="card">
                                        <div>
                                            <xsl:attribute name="class">
                                                card-header <xsl:if test="count(file) = '0'">bg-success</xsl:if>
                                                <xsl:if test="count(file) > '0'">bg-danger</xsl:if>
                                            </xsl:attribute>
                                            <strong>
                                            <xsl:if test="file/@errors > 0"><xsl:value-of select="file/@errors" /><xsl:text> </xsl:text>Error(s)</xsl:if> <xsl:text> </xsl:text>
                                            <xsl:if test="file/@warnings > 0"><xsl:value-of select="file/@warnings" /><xsl:text> </xsl:text>Warning(s)</xsl:if> <xsl:text> </xsl:text>
                                            <xsl:if test="file/@fixables > 0"><xsl:value-of select="file/@fixables" /><xsl:text> </xsl:text>Fixable</xsl:if> <xsl:text> </xsl:text>
                                            </strong>
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <xsl:if test="count(file) = '0'">
                                                <li class="list-group-item">No errors found.</li>
                                            </xsl:if>
                                            <xsl:apply-templates select="file" mode="phpcs" />
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </xsl:for-each>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="/assets/js/mapbox-gl.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript" src="/assets/js/jquery-3.2.1.min.js"><xsl:comment></xsl:comment></script>
            <script type="text/javascript" src="/assets/js/behavior.min.js"><xsl:comment></xsl:comment></script>
        </body>
        </html>
    </xsl:template>
    
    <xsl:template match="file" mode="phpmd">
        <li class="list-group-item">
            <xsl:value-of select="@name" /> (Line <xsl:value-of select="violation/@beginline" /> - <xsl:value-of select="violation/@endline" />)<br />
            <xsl:value-of select="violation" /><br />
            <xsl:if test="violation/@externalInfoUrl != '#'">
            <a href="{violation/@externalInfoUrl}"><xsl:value-of select="violation/@externalInfoUrl" /></a>
            </xsl:if>
        </li>
    </xsl:template>
    
    <xsl:template match="file" mode="phpcs">
        <li class="list-group-item">
            <xsl:value-of select="@name" /><br />
            <xsl:value-of select="violation" /><br />
            <xsl:for-each select="error">
                Error on line: <xsl:value-of select="@line" />, Column: <xsl:value-of select="@column" /><xsl:text> </xsl:text><xsl:value-of select="." /><br />
            </xsl:for-each>
            <xsl:for-each select="warning">
                Warning on line: <xsl:value-of select="@line" />, Column: <xsl:value-of select="@column" /><xsl:text> </xsl:text><xsl:value-of select="." /><br />
            </xsl:for-each>
        </li>
    </xsl:template>
</xsl:stylesheet>