<?php
/**
 * Main cli-config.php file.
 *
 * @category File
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 *
 */
declare(strict_types = 1);

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Ramsey\Uuid\Doctrine\UuidType;

require_once "vendor/autoload.php";

$config = include __DIR__ . '/../config/config.php';

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$ormConfig = Setup::createAnnotationMetadataConfiguration([__DIR__."/../src/Entity"], $isDevMode);
// or if you prefer yaml or XML
// $ormConfig = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), $isDevMode);
//$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);

// database configuration parameters
$conn = [
    'dbname' => $config['db']['database'],
    'user' => $config['db']['user'],
    'password' => $config['db']['pass'],
    'host' => $config['db']['host'],
    'driver' => $config['db']['driver'],
    'charset' => 'utf8mb4',
    'default_table_options' => [
        'charset' => 'utf8mb4',
        'collate' => 'utf8mb4_unicode_ci',
    ],
];
// $ormConfig->setD['driver'] = $config['db']['driver'];
// obtaining the entity manager
\Doctrine\DBAL\Types\Type::addType('uuid', '\Ramsey\Uuid\Doctrine\UuidType');
\Doctrine\DBAL\Types\Type::addType('point', '\CrEOF\Spatial\DBAL\Types\Geometry\PointType');
print_r(\Doctrine\DBAL\Types\Type::getTypesMap());
$entityManager = EntityManager::create($conn, $ormConfig);

// return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);
return ConsoleRunner::createHelperSet($entityManager);
