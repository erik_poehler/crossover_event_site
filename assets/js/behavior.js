/**
 * Website Behavior Javascript
 * 
 * @package EventSite
 * @author  Erik Pöhler <info@teuton.mx>
 * @license CC-BY-NC-ND-4.0
 * @link    https://www.erikpoehler.com/
 */

mapboxgl.accessToken = 'pk.eyJ1IjoidGV1dG9uIiwiYSI6ImNqOTFtajd6dTM4bWsyem1ydG1oeDBkbGkifQ.YfrMiRxSjnTIpZc6CBRoBg';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    center: [-121.8939647, 37.3306844],
    zoom: 14
});
//Center the map on the coordinates of any clicked symbol from the 'symbols' layer.
map.on('click', 'symbols', function (e) {
    map.flyTo({center: e.features[0].geometry.coordinates});
});
/**
// Change the cursor to a pointer when the it enters a feature in the 'symbols' layer.
map.on('mouseenter', 'symbols', function () {
    map.getCanvas().style.cursor = 'pointer';
});

// Change it back to a pointer when it leaves.
map.on('mouseleave', 'symbols', function () {
    map.getCanvas().style.cursor = '';
});
**/