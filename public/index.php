<?php
/**
 * Main index.php file. Depends on mod_rewrite enabled and .htaccess file
 *
 * @category File
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 *
 */
declare(strict_types = 1);

require __DIR__ . '/../src/Bootstrap.php';
