<?php
/**
 * Postal Address Entity
 *
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 *
 */
declare(strict_types = 1);

namespace EventSite\Entity;

use Ramsey\Uuid\Uuid;
use CrEOF\Spatial\PHP\Types\Geography\Point;
use Doctrine\Tests\DBAL\Types\DateTest;

/**
 * @Entity
 * @Table(name="addresses")
 */
class Address
{
    /**
     * @var \Ramsey\Uuid\Uuid
     *
     * @Id
     * @Column(type="uuid", unique=true, nullable=false)
     * @GeneratedValue(strategy="CUSTOM")
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $address_id;

    /**
     * @Column(type="string", nullable=false)
     * @var string
     */
    private $street;

    /**
     * @Column(type="string", nullable=false, length=255)
     * @var string
     */
    private $street_exterior;

    /**
     * @Column(type="string", nullable=true)
     * @var float
     */
    private $interior_no;

    /**
     * @Column(type="string", nullable=false)
     * @var float
     */
    private $locality;
    /**
     * @Column(type="string", nullable=false)
     * @var float
     */
    private $state;

    /**
     * @Column(type="string", nullable=false)
     * @var float
     */
    private $state_short;

    /**
     * @Column(type="string", nullable=false)
     * @var float
     */
    private $country_name;

    /**
     * @Column(type="string", nullable=false)
     * @var float
     */
    private $zip_code;

    /**
     * @Column(type="datetime", nullable=true)
     * @var
     */
    private $created_at;

    /**
     *
     * @param string $country_name
     * @param string $interior_no
     * @param string $locality
     * @param string $state
     * @param string $state_short
     * @param string $street
     * @param string $street_exterior
     * @param string $zip_code
     * @return void
     */
    public function __construct($country_name, $interior_no = null, $locality, $state, $state_short, $street, $street_exterior, $zip_code)
    {
        $this->address_id = Uuid::uuid4();
        $this->country_name = $country_name;
        $this->interior_no = $interior_no;
        $this->locality = $locality;
        $this->state = $state;
        $this->state_short = $state_short;
        $this->street = $street;
        $this->street_exterior = $street_exterior;
        $this->zip_code = $zip_code;
        $this->created_at = new \DateTime('now');
    }

    /**
     *
     * @return string
     */
    public function getId()
    {
        return $this->address_id->__toString();
    }

    /**
     *
     * @return array
     */
    public function __toArray() : array
    {
        return [
            'address_id' => $this->address_id,
            'country_name' => $this->country_name,
            'interior_no' => $this->interior_no,
            'locality' => $this->locality,
            'state' => $this->state,
            'state_short' => $this->state_short,
            'street' => $this->street,
            'street_exterior' => $this->street_exterior,
            'zip_code' => $this->zip_code,
            'created_at' => $this->created_at->format(\DateTime::ISO8601),
        ];
    }
}
