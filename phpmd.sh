./vendor/bin/phpmd ./src xml codesize ./phpmd.xml --reportfile ./cache/phpmd.codesize.xml
./vendor/bin/phpmd ./src xml cleancode ./phpmd.xml --reportfile ./cache/phpmd.cleancode.xml
./vendor/bin/phpmd ./src xml controversial ./phpmd.xml --reportfile ./cache/phpmd.controversial.xml
./vendor/bin/phpmd ./src xml design ./phpmd.xml --reportfile ./cache/phpmd.design.xml
./vendor/bin/phpmd ./src xml naming ./phpmd.xml --reportfile ./cache/phpmd.naming.xml
./vendor/bin/phpmd ./src xml unusedcode ./phpmd.xml --reportfile ./cache/phpmd.unusedcode.xml
