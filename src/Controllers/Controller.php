<?php
/**
 * Dependency Management
 *
 * @desc     Defines dependencies for various required classes
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 */
declare(strict_types = 1);

namespace EventSite\Controllers;

use Symfony\Component\HttpFoundation\Session\Session;
use InvalidArgumentException;

class Controller
{
    public function isLoggedIn() : bool
    {
        return ($this->session->get('name') === 'crossover') ? true : false;
    }

    /**
     * Login a user
     * @param array $user
     * @throws InvalidArgumentException
     * @return void
     */
    public function doLogin(array $user) : void
    {
        if (!is_array($user)) {
            throw new InvalidArgumentException();
        }
        if (!count($user)) {
            throw new InvalidArgumentException();
        }
        $this->session->set('name', 'crossover');
        $this->session->set('user', $user);
        $this->session->getFlashBag()->add('loggedin', '1');
    }

    /**
     * Logs out the user
     * @return void
     */
    public function doLogout() : void
    {
        $this->session->clear();
    }

    /**
     * Gets the username of the current user from the session
     * @param string $default
     * @return string
     */
    public function getUsername($default = 'guest') : string
    {
        if (is_null($this->session->get('user')['username'])) {
            return $default;
        }
        return (string) $this->session->get('user')['username'];
    }

    /**
     * Returns the users' Full Name
     * @param string $default
     * @return string
     */
    public function getUserFullname($default = 'Guest') : string
    {
        if (is_null($this->session->get('user')['fullname'])) {
            return $default;
        }
        return (string) $this->session->get('user')['fullname'];
    }

    /**
     * Retrieves the complete session. null if not set
     * @return Session|NULL
     */
    public function getSession() : ?Session
    {
        if ($this->session->get('name') === 'crossover') {
            return $this->session;
        }
        return null;
    }

    /**
     * Retrieves all parameters from the session required for xslt rendering
     * @return array
     */
    public function getParameters() : array
    {
        if ($this->session->get('name') === 'crossover') {
            return $this->session->getFlashBag()->all();
        }
        return [];
    }
}
