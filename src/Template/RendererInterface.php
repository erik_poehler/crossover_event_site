<?php
/**
 * Renderer Interface
 *
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 */
declare(strict_types = 1);

namespace EventSite\Template;

use XSLTProcessor;
use DOMDocument;
use Doctrine\ORM\EntityManager;

interface RendererInterface
{
    public function __construct(XSLTProcessor $engine, EntityManager $em = null);
    public function render($template, $layout = 'default', $params = [], $cache = false, DOMDocument $dom = null) : string;
}
