<?php
/**
 * EventDate Entity
 *
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 *
 */
declare(strict_types = 1);

namespace EventSite\Entity;

use Ramsey\Uuid\Uuid;
use EventSite\Entity\Event;

/**
 * @Entity
 * @Table(name="event_dates")
 */
class EventDate
{
    const TYPE_START = 'start';
    const TYPE_END = 'end';

    /**
     * @var \Ramsey\Uuid\Uuid
     *
     * @Id
     * @Column(type="uuid", unique=true, nullable=false, options={"comment":"Event Date ID"})
     * @GeneratedValue(strategy="CUSTOM")
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $event_date_id;

    /**
     * @Column(type="datetime", nullable=true, options={"comment":"Datetime"})
     */
    private $date;

    /**
     * @Column(type="string", nullable=false, options={"comment":"Enum either start or end"})
     * @var string
     */
    private $type;

    /**
     * @ManyToOne(targetEntity="Event")
     * @JoinColumn(name="fk_event", nullable=false, referencedColumnName="event_id")
     * @var int
     */
    private $fk_event;

    /**
     * @Column(type="datetime", nullable=true, options={"comment":"Date created"})
     * @var
     */
    private $created_at;

    /**
     *
     * @param DateTime $date
     * @param string $type
     * @param EventSite\Entity\Event $event
     * @return \EventSite\Entity\EventDate
     */
    public function __construct(\DateTime $date, $type, Event $event)
    {
        $this->event_date_id = Uuid::uuid4();
        $this->date = $date;
        $this->type = $type;
        $this->fk_event = $event;
        $this->created_at = new \DateTime("now");
    }

    /**
     *
     * @return string
     */
    public function getId()
    {
        return $this->event_date_id->__toString();
    }

    /**
     *
     * @return array
     */
    public function __toArray() : array
    {
        return [
            'event_date_id' => $this->event_date_id->__toString(),
            'date' => $this->date->format(\DateTime::ISO8601),
            'type' => $this->type,
            'event' => $this->fk_event,
            'created_at' => $this->created_at->format(\DateTime::ISO8601),
        ];
    }
}
