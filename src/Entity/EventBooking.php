<?php
/**
 * EventBooking Entity
 *
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 *
 */
declare(strict_types = 1);

namespace EventSite\Entity;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use EventSite\Entity\User;
use EventSite\Entity\Event;

/**
 * @Entity
 * @Table(name="event_bookings")
 */
class EventBooking
{
    /**
     * @var \Ramsey\Uuid\Uuid
     *
     * @Id
     * @Column(type="uuid", unique=true, nullable=false, options={"comment":"Event Booking ID"})
     * @GeneratedValue(strategy="CUSTOM")
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $event_booking_id;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", nullable=false, referencedColumnName="user_id")
     * @var EventSite\Entity\User
     */
    private $user;

    /**
     * @ManyToOne(targetEntity="Event")
     * @JoinColumn(name="event", nullable=false, referencedColumnName="event_id")
     * @var EventSite\Entity\Event
     */
    private $event;

    /**
     * @Column(type="datetime", nullable=true, options={"comment":"Date created"})
     * @var
     */
    private $created_at;

    /**
     *
     * @param User $user
     * @param Event $event
     */
    public function __construct(User $user, Event $event)
    {
        $this->event_booking_id = Uuid::uuid4();
        $this->user = $user;
        $this->event = $event;
        $this->created_at = new \DateTime("now");
    }

    /**
     *
     * @return string
     */
    public function getId()
    {
        return $this->event_booking_id->__toString();
    }

    /**
     *
     * @return array
     */
    public function __toArray() : array
    {
        return [
            'event_booking_id' => $this->event_booking_id->__toString(),
            'user' => $this->user,
            'event' => $this->event,
            'created_at' => $this->created_at->format(\DateTime::ISO8601),
        ];
    }
}
