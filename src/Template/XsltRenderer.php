<?php
/**
 * Xslt Renderer Class
 *
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     https://www.erikpoehler.com/
 */
declare(strict_types = 1);

namespace EventSite\Template;

use DOMDocument;
use XSLTProcessor;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use EventSite\Repository\EventRepository as Events;
use EventSite\Template\RendererInterface;
use EventSite\Entity\Venue;
use EventSite\Entity\Event;

class XsltRenderer implements RendererInterface
{
    private $engine;
    private $params = [];
    /**
     *
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct(\XSLTProcessor $engine, EntityManager $entityManager = null)
    {
        $this->engine = $engine;
        $this->entityManager = $entityManager;
    }

    public function render($template, $layout = 'default', $params = [], $cache = false, DOMDocument $dom = null) : string
    {
        // accept namespace::template syntax for defining templates
        $parts = explode('::', $template);
        $template = implode('/', $parts);

        // check if the html has been cached
        $cachepath = realpath(__DIR__ . "/../../cache") . '/' . md5($template . $layout) . '.html';
        if ($cache && is_file($cachepath)) {
            return file_get_contents($cachepath);
        }

        // load the template
        $template = realpath(__DIR__ . "/../../templates/$template.xsl");
        if (empty($template) || !is_file($template)) {
            throw new \Exception(sprintf('Template <samp>%s</samp> not found.', $template));
        }

        // import extensible stylesheet
        $style = new DOMDocument();
        $style->load($template);
        $this->engine->importStylesheet($style);

        // build or get the xml data document (only when not passed via parameter)
        if (is_null($dom)) {
            $dom = $this->getData($cache);
        }

//         $this->prepareParams($params);
        $html = '<!DOCTYPE html>' . $this->engine->transformToXml($dom);

        // save the html to the cache
        if ($cache) {
            file_put_contents($cachepath, $html);
        }
        return $html;
    }

    /**
     * Retrieves the data xml document
     * @param array $data
     * @param bool $cache
     * @return DOMDocument
     */
    private function getData(bool $cache = false) : DOMDocument
    {
        // check if the data has been cached
        $cachepath = realpath(__DIR__ . "/../../cache") . '/data.xml';
        $dom = new DOMDocument();
        if ($cache && is_file($cachepath)) {
            $dom->load($cachepath);
            return $dom;
        }

        // create a new XML document
        $dom->loadXml('<root></root>');
        $events = $this->entityManager->getRepository('EventSite\Entity\Event')->findAll();
        foreach ($events as $event) {
            $classshortname = substr(get_class($event), strrpos(get_class($event), '\\')+1);
            $this->createNodesFromData($dom, $dom->documentElement, $event->__toArray(), $classshortname);
        }

        // cache it
//         if ($cache) {
            file_put_contents($cachepath, $dom->saveXML());
//         }
//         echo "<textarea style=\"width:100%;height:200px;\">".$dom->saveXml($dom->documentElement)."</textarea>";
        return $dom;
    }

    private function createNodesFromData($dom, $parent, $data, $classshortname)
    {
        $node = $dom->createElement(ucwords($classshortname));
        foreach ($data as $key => $value) {
            if (gettype($value) === 'string' && in_array($key, [strtolower($classshortname).'_id','created_at'])) {
                $node->setAttribute(str_replace(strtolower($classshortname).'_', '', $key), $value); // assumes is always a string
            } else {
                switch (gettype($value)) {
                    case 'string':
                        $cdata = $dom->createCDATASection($value);
                        $child = $dom->createElement($key);
                        $child->appendChild($cdata);
                        $node->appendChild($child);
                        break;
                    case 'double':
                        $child = $dom->createElement($key, htmlentities((string) $value));
                        $node->appendChild($child);
                        break;
                    case 'array':
                        $this->createNodesFromData($dom, $node, $value, $key); // recursive processing
                    case 'NULL':
                    case 'object':
                    default:
                        break;
                }
            }
        }
        $parent->appendChild($node);
    }

    /**
     *
     * @param array $params
     * @return void
     */
    private function prepareParams($params = [])
    {
        // set parameters
        //         $this->params = $params;

        //         foreach ($this->session->getFlashBag()->all() as $type => $messages) {
        //             foreach ($messages as $message) {
        //                 $this->engine->setParameter('', [$type => $message]);
        //             }
        //         }
        //         $session = new Session();
        //         var_dump($session->all());
        //         die;
        //         foreach ($this->session->all() as $key => $value) {
        //             if (in_array(gettype($value), ['string','int', 'bool'])) {
        //                 echo $key . ' '. $value . '<br>';
        //                 die;
        //                 $this->engine->setParameter('', [$key => $value]);
        //             }
        //         }

        if (!empty($params)) {
            foreach ($this->params as $param => $value) {
                $this->engine->setParameter('', [$param => $value]);
            }
        }
    }
}
