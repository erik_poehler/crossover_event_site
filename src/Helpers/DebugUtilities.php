<?php
/**
 * Debugging Helper
 *
 * @desc     autoloading, error handling, dependency injection, route dispatching
 * @category Class
 * @package  EventSite
 * @license  CC-BY-NC-ND-4.0
 * @author   Erik Pöhler <info@teuton.mx>
 * @link     http://www.erikpoehler.com/
 */
declare(strict_types = 1);

namespace EventSite\Helpers;

class DebugUtilities
{
    private $durationStart;
    private $durationEnd;

    /**
     * prints $input in a textarea
     * @param array|object $input
     * @param string $height
     * @param string $width
     * @param string $id
     */
    public static function printRInTextarea($input, $height = "300px", $width = "100%", $id = "")
    {
        $idXHTML = "";
        if ($id != "") {
            $idXHTML = ' id="'.$id.'" ';
        }
        echo "<textarea $idXHTML style=\"width:$width;height:$height\">";
        if (ob_start()) {
            print_r($input);
            $res = ob_get_clean();
            echo htmlentities($res);
        }
        echo "</textarea>";
    }

    /**
     * echoes $input in a textarea
     * @param string $input
     * @param string $height
     * @param string $width
     * @param string $id
     * @param string $name
     */
    public static function showInTextarea($input, $height = "300px", $width = "100%", $id = "", $name = "")
    {
        if ($id != "") {
            $idXHTML = ' id="'.$id.'" ';
        }
        if ($name != "") {
            $idXHTML = ' name="'.$name.'" ';
        }
        echo "<textarea $idXHTML style=\"width:$width;height:$height\">".htmlentities($input)."</textarea>";
    }

    /**
     * print_r $input in <pre> tag
     * @param array|object $input
     */
    public static function printRInPreTags($input)
    {
        echo "<pre>";
        print_r($input);
        echo "</pre>";
    }

    /**
     * Show relevant configuring
     *
     * @see http://de2.php.net/manual/de/ref.errorfunc.php
     */
    public static function showErrorReportingConfig()
    {
        echo ini_get("display_errors");
        echo error_reporting();
    }
    /**
     * Show relevant configuring
     *
     * @see http://de2.php.net/manual/de/ref.errorfunc.php
     */
    public static function setErrorReportingOnAnyway($strict = true)
    {
        ini_set("display_errors", "On");
        if ($strict) {
            error_reporting(E_STRICT);
        } else {
            error_reporting(E_ALL);
        }
    }

    /**
     * Starts a timer
     * @return void
     */
    public function startDuration() : void
    {
        $this->durationStart = microtime(true);
    }

    /**
     * Stops the timer
     * @return void
     */
    public function endDuration() : void
    {
        $this->durationEnd = microtime(true);
    }

    /**
     * Stops the timer and returns output
     * @return void
     */
    public function endDurationAndReturn() : string
    {
        $this->endDuration();
        return $this->showDurationStatistics();
    }

    /**
     * Get some profiling stats
     * @return string
     */
    public function showDurationStatistics() : string
    {
        $out = "Start: ".$this->getMicrotimeLine($this->durationStart)."\n";
        $out .= "End: ".$this->getMicrotimeLine($this->durationEnd)."\n";
        $out .= "Rendered in: ".number_format(($this->durationEnd - $this->durationStart)*1000, 1) . " ms\n";
        return $out;
    }

    public static function backtraceInPreTags()
    {
        if (ob_start()) {
            debug_print_backtrace();
            self::print_rInPreTags(ob_get_clean());
        }
    }

    public static function backtraceShortInPreTags()
    {
        foreach (debug_backtrace() as $data) {
            $c .= $data["file"].":".$data["line"]."\t".$data["object"].":".$data["class"]."::".$data["function"]."(".implode(",", $data["args"]).")\n";
        }
        self::print_rInPreTags($c);
    }

    /**
     * Microtime conversion
     * @param float $microtime
     * @return string
     */
    private function getMicrotimeLine(float $microtime) : string
    {
        $mtstring = (string) $microtime;
        return date("H:i:s", intval($microtime)).", ".substr($mtstring, 10);
    }

    /**
     * Transform bytes into human readable filesize with unit
     *
     * @param  integer $bytes     size of the file in bytes
     * @param  integer $precision after-comma precision
     * @return string
     */
    public function bytesToSize($bytes, $precision = 2) : string
    {
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        $terabyte = $gigabyte * 1024;

        if (($bytes >= 0) && ($bytes < $kilobyte)) {
            return $bytes . ' B';
        } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
            return round($bytes / $kilobyte, $precision) . ' KB';
        } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
            return round($bytes / $megabyte, $precision) . ' MB';
        } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
            return round($bytes / $gigabyte, $precision) . ' GB';
        } elseif ($bytes >= $terabyte) {
            return round($bytes / $terabyte, $precision) . ' TB';
        } else {
            return $bytes . ' B';
        }
    }
}
